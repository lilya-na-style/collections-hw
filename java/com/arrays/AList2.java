package com.arrays;

import com.interf.IList2;

import java.util.Arrays;

public class AList2<T> implements IList2<T> {

    private static final int DEFAULT_CAPACITY=10;
    private int size;
    private T[] elementData;

    @Override
    public void clear() {
        size=0;
        ensureCapacity(DEFAULT_CAPACITY);
    }

    @Override
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return size()==0;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new RuntimeException ();
        }
    }

    // Повторно выделяем длину массива, for используется для копирования данных
    private void ensureCapacity(int newCapacity) {
        if (newCapacity<size){
            return;
        }
        T[] old = elementData;
        elementData = (T[]) new Object[newCapacity];
        for (int i = 0;i < size();i++) {
            elementData[i] = old[i];
        }
    }

    @Override
    public boolean add(T element) {
        grow();
        elementData[size] = element;
        size++;
        return true;
    }

    private void grow() {
        if (size == realSize()) {
            T[] tempData = (T[]) new Object[size + (size >> 1)];
            System.arraycopy(elementData, 0, tempData, 0, elementData.length);
            elementData = tempData;
        }
    }

    public int realSize() {
        return elementData.length;
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        // изменяем возвращаемое значение
        return elementData[index];
    }

    // Добавляем элементы по указанному индексу
    public boolean add(int index, T x) {
        checkIndex(index);
        grow();
        System.arraycopy(elementData, index, elementData, index + 1, size - index);
        elementData[index] = x;
        size++;
        return true;

    }

    @Override
    public T removeByIndex(int index){
        T removedItem = elementData[index];
        for (int i = index; i<size()-1; i++){
            elementData[i] =elementData[i+1];
        }
        size--;
        return removedItem;
    }

    // Удаляем элементы через объект
    @Override
    public boolean remove(T element) {
        int tempIndex = -1;
        for (int i = 0; i < size; i++) {
            if (element == (elementData[i])) {
                tempIndex = i;
                break;
            }
        }
        if (tempIndex != -1) {
            remove(element);
        }
        return false;
    }

    @Override
    public boolean contains(T value) {
        for (int i = 0; i < size; i++) {
            if (elementData[i] == null) {
                if (value == null) {
                    return true;
                } else {
                    continue;
                }
            }
            if (elementData[i].equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, T value) {
        if (index >= size) {
            return false;
        }
        elementData[index] = value;
        return true;
    }

    @Override
    public boolean removeAll(T[] ar) {
        if (elementData == null) {
            return false;
        }
        for (int i = 0; i < elementData.length; i++) {
            if (contains(elementData[i])) {
                remove(elementData[i]);
            }
        }
        return true;
    }

    @Override
    public boolean print() {
        System.out.println("["+ Arrays.toString(elementData)+"]");

        return true;
    }

    @Override
    public T[] toArray() {
        return Arrays.copyOf(elementData, size);
    }

    public static void main(String[] args) {

    }





}
