package com.arrays;

import com.interf.IList;

import java.util.Arrays;

public class AList1 implements IList {

    private int[] elementData;
    private int size;
    private static final int DEFAULT_CAPACITY = 10;

    public AList1() {
        elementData = new int[DEFAULT_CAPACITY];
    }

    public AList1(int capacity) {
        if (capacity < 0) {
            throw new RuntimeException("capacity cant be < 0");
        } else if (capacity == 0) {
            elementData = new int[DEFAULT_CAPACITY];
        } else {
            elementData = new int[capacity];
        }
    }

    // Инициализируем массив так, чтобы длина массива по умолчанию была 10
    @Override
    public void clear() {
        size = 0;
        ensureCapacity(DEFAULT_CAPACITY);
    }

    // Повторно выделяем длину массива, цикл for используется для копирования данных
    private void ensureCapacity(int newCapacity) {
        if (newCapacity < size) {
            return;
        }
        int[] old = elementData;
        elementData = new int[newCapacity];
        for (int i = 0; i < size(); i++) {
            elementData[i] = old[i];
        }
    }

    @Override
    public boolean add(int index, int element) {
        if (elementData.length == size()) {
            ensureCapacity(size() * 2 + 1);
        }
        for (int i = size; i > index; i--) {
            elementData[i] = elementData[i - 1];
        }
        elementData[index] = element;

        size++;
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0 ? true : false;
    }

    @Override
    public boolean add(int element) {
        if (size == elementData.length) {
            int[] newArray = new int[elementData.length + (elementData.length >> 1)];
            System.arraycopy(elementData, 0, newArray, 0, elementData.length);
            elementData = newArray;
        }
        elementData[size++] = element;
        return false;
    }


    @Override
    public int get(int index) {
        checkRange(index);
        return (int) elementData[index];
    }

    public void checkRange(int index) {
        if (index < 0 || index > size - 1) {
            throw new RuntimeException("wrong index:" + index);
        }
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < size; i++) {
            if (elementData[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int removeByIndex(int index) {
        int numMoved = elementData.length - index - 1;
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);
        }
        elementData[--size] = Integer.parseInt(null);
        return numMoved;
    }

    @Override
    public boolean remove(int element) {
        for (int i = 0; i < size; i++) {
            if (element == (get(i))) {
                remove(i);
            }
        }
        return false;
    }

    public boolean set(int index, int newVal) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int old = elementData[index];
        elementData[index] = newVal;
        System.out.println(old);

        return true;
    }

    @Override
    public boolean removeAll(int[] elementData) {
        elementData = new int[0];
        return true;
    }

    @Override
    public boolean print() {
        System.out.println("["+Arrays.toString(elementData)+"]");

        return true;
    }

    @Override
    public int[] toArray() {
        return Arrays.copyOf(elementData, size);
    }

    public static void main(String[] args) {

    }


}
