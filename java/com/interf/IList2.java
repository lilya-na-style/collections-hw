package com.interf;

public interface IList2 <T> {

    void clear();

    int size();

    T get(int index);

    boolean add(T x);

    boolean add(int index, T value);

    boolean remove(T number);

    T removeByIndex(int index);

    boolean set(int index, T value);

    boolean removeAll(T[] ar);

    boolean contains(T value);

    boolean print(); // выводит в консоль массив в квадратных скобках и через запятую

    T[] toArray(); // приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив

}

